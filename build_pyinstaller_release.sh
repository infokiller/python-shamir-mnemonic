#!/usr/bin/env bash

# See https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -o errexit -o errtrace -o nounset -o pipefail

SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
cd -- "${SCRIPT_DIR}"

pyinstaller --noconfirm --onefile --log-level=WARN \
  --name slip39-cli \
  --add-data shamir_mnemonic/wordlist.txt:shamir_mnemonic \
  ./python-shamir-mnemonic.py
